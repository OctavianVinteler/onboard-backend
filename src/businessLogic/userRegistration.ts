/* ---------------------------- IMPORTS ------------------------------------- */
const TypingDnaClient = require('typingdnaclient');

/* ------------------------ PROJECT IMPORTS --------------------------------- */
import {
  VERIFY_QUALITY,
  AUTOENROLL_TRESHOLD
} from './constants';

/* --------------------------- CODE BASE ------------------------------------ */
export function registerUser({
  userId,
  pattern1,
  pattern2,
  textId,
  device,
  callback
}: {
    userId: string,
    pattern1: string,
    pattern2: string,
    textId: string,
    device: string
    callback: Function
  }) {
  // Read config values
  let config = require('../config/config.json');
  const { apiKey, apiSecret } = config;

  let typingDnaClient = new TypingDnaClient(apiKey, apiSecret);

  typingDnaClient.check({
    userId,
    type: '1',
    textId,
    device
  }, (error: any, result: any) => {
    if (error) {
      throw error;
    } else if (result.success === 0) {
      throw new Error(result.message)
    } else {
      // New user or user with just one pattern
      if (result.count < 2) {
        // register the first pattern
        typingDnaClient.save(userId, pattern1,
          (error: any, result: any) => {
            if (error) {
              throw error;
            } else if (result.success === 0) {
              throw new Error(result.message)
            } else {
              // register the second pattern
              typingDnaClient.save(userId, pattern2,
                (error: any, result: any) => {
                  if (error) {
                    throw error;
                  } else if (result.success === 0) {
                    throw new Error(result.message)
                  } else {
                    return true;
                  }
                });
            }
          });
        // User already exists, just verify
      } else {
        let pattern1Score: number;
        let pattern2Score: number;
        let pattern1Result: boolean;
        let pattern2Result: boolean;

        // Verify the first pattern
        typingDnaClient.verify(userId, pattern1, VERIFY_QUALITY,
          (error: any, result: any) => {
            if (error) {
              throw error;
            } else if (result.success === 0) {
              throw new Error(result.message)
            } else {
              pattern1Score = result.score;
              pattern1Result = result.result;
              console.log('result-here1', result);
              // Verify the second pattern
              typingDnaClient.verify(userId, pattern2, VERIFY_QUALITY,
                (error: any, result: any) => {
                  if (error) {
                    throw error;
                  } else if (result.success === 0) {
                    throw new Error(result.message)
                  } else {
                    console.log('result-here2', result);
                    pattern2Score = result.score;
                    pattern2Result = result.result;

                    // if score for pattern1 > AUTOENROLL_TRESHOLD, save pattern
                    if (pattern1Score > AUTOENROLL_TRESHOLD) {
                      typingDnaClient.save(userId, pattern1);
                    }

                    // if score for pattern1 > AUTOENROLL_TRESHOLD, save pattern
                    if (pattern2Score > AUTOENROLL_TRESHOLD) {
                      typingDnaClient.save(userId, pattern2);
                    }

                    // if score for pattern1 > 90, save pattern
                    typingDnaClient.save(userId, pattern2);

                    callback(pattern1Result && pattern2Result);
                  }
                });
            }
          });
      }
    }
  });
}
