/* ---------------------------- IMPORTS ------------------------------------- */
import express, { Request, Response } from 'express';

/* ------------------------ PROJECT IMPORTS --------------------------------- */
import { registerUser } from '../businessLogic/userRegistration';

/* --------------------------- CODE BASE ------------------------------------ */
const router = express.Router();

router.get('/', (_req, res) =>
  res.sendStatus(200));

router.post('/', (req: Request, res: Response) => {
  const { userId, pattern1, pattern2, textId, device } = req.body;

  registerUser({
    userId, pattern1, pattern2, textId, device,
    callback: (result: boolean) =>
      res.send({
        result
      })
  });
});

export default router;
