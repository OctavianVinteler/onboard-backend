/* ---------------------------- IMPORTS ------------------------------------- */
import express from 'express';
import bodyParser from 'body-parser';
import router from './routes';

/* --------------------------- CODE BASE ------------------------------------ */
const API_PORT = 3001;
const app = express();
app.use(function(req, res, next) {
  if (req.headers.origin) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Authorization')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE,OPTIONS')
  }
  next()
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', router);

app.listen(API_PORT, () => {
  console.log(`Server listening on port ${API_PORT}!`);
});
