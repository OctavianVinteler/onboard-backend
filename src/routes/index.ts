/* ---------------------------- IMPORTS ------------------------------------- */
import express from 'express';

/* ------------------------ PROJECT IMPORTS --------------------------------- */
import registrationRouter from './registration';

/* --------------------------- CODE BASE ------------------------------------ */
const router = express.Router();

router.use('/registration', registrationRouter);

export default router;
